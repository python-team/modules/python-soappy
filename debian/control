Source: python-soappy
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Bernd Zeimetz <bzed@debian.org>
Build-Depends: debhelper (>= 10~),
               cdbs (>= 0.4.90~),
               python-all (>= 2.6.6-3~),
               dh-python,
               python-setuptools
Vcs-Git: https://salsa.debian.org/python-team/packages/python-soappy.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-soappy
Standards-Version: 3.9.5
Homepage: https://github.com/kiorky/SOAPpy.git

Package: python-soappy
Architecture: all
Depends: ${python:Depends},
         ${misc:Depends}
Description: SOAP Support for Python
 This package contains the SOAPpy Python module, which implements a high-
 level interface to Simple Object Access Protocol (SOAP) functions from
 Python. It includes functionality for acting as a SOAP server, client,
 or proxy, and includes both building and parsing functions. It has been
 tested against a wide array of other SOAP clients and servers, and is a very
 robust and well-rounded implementation of the SOAP protocol.
 .
 SOAPpy is not actively supported by upstream anymore - please do not
 consider to use it in new projects! All functions were merged into ZSI,
 which is available in the python-zsi package, use it instead.
